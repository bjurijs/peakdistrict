export default {
  'LaunchRequest': (channel) => {
    let say = 'Welcome!'
    channel.speak(say).Ready()
  },

  'WelcomeIntent': (channel) => {
    let say = 'Hi, how can I help you?'
    channel.speak(say).Ready()
  },

  'AMAZON.HelpIntent': (channel) => {
    let say = 'Help section.'
    channel.speak(say).Ready()
  },

  'AMAZON.CancelIntent': (channel) => {
    let say = 'Ok.'
    channel.speak(say).Ready()
  },

  'AMAZON.StopIntent': (channel) => {
    let say = 'Bye.'
    channel.speak(say).endSession().Ready()
  },

  'Unhandled': (channel) => {
    let say = 'Error.'
    channel.speak(say).endSession().Ready()
  },

  'SessionEndedRequest': (channel) => {
    channel.endSession().Ready()
  }
}
