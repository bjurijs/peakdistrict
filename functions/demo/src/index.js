import peakdistrict, { initialize } from 'peakdistrict'
import mainHandler from './handlers/main.handler'

exports.handler = (event, context, callback) => {
  const channel = initialize(event, callback)
  peakdistrict.registerHandlers(mainHandler)
  peakdistrict.getHandler(channel)
}
