export default function get (event, key) {
  /**
  * Checking each key in path and returning undefined if
  * some of them is undefined. Made to avoid errors
  * such as "can't read 'name' of undefined"
  * @params event – entry key
  * @params key – actual path
  * returns value of the key or undefined
  */
  return key.split('.').reduce((x, y) => {
    return (typeof x === 'undefined' || x === null) ? x : x[y]
  }, event)
}
