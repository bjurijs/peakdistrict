export default function removeSSML (x) {
  /**
  * Filtering SSML tags
  * @params x – string with SSML tags
  * returns string without any tags
  */
  return x.replace(/<\/?[^>]+(>|$)/g, '')
}
