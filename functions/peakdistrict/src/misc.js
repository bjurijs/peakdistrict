function removeSSML (x) {
  return x.replace(/<\/?[^>]+(>|$)/g, '')
}

function get (event, key) {
  return key.split('.').reduce(function (x, y) {
    return (typeof x === 'undefined' || x === null) ? x : x[y]
  }, event)
}

export { removeSSML, get }
