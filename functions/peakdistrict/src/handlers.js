export default class Handlers {
  constructor () {
    /**
    * Preparing empty array for our handlers.
    */
    this.handlers = []
  }

  registerHandlers () {
    /**
    * Collecting handlers to store all of them in array
    * for later use.
    */
    for (var x = 0; x < arguments.length; x++) {
      const handlerObject = arguments[x]
      this.handlers.push(handlerObject)
    }
  }

  requestHandler (state) {
    /**
    * Looking for right handler by state.
    * @params state function name which is stored in handler we looking for.
    * Most of the times state = intent name.
    */
    for (let handler in this.handlers) {
      for (let intent in this.handlers[handler]) {
        if (state === intent) {
          return this.handlers[handler]
        }
      }
    }
  }

  getHandler (channel) {
    let state = channel.attributes['state']
    let requestHandler = this.requestHandler(state)

    return requestHandler
      ? requestHandler[state](channel)
      : this.requestHandler('Unhandled')['Unhandled'](channel)
  }
}
