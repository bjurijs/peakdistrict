import Handlers from './handlers'
export default new Handlers()
export { default as initialize } from './factory'
