export default {
  ERROR_COMMON: 'There was a problem.',
  ERROR_PLATFORM_UNSUPPORTED: 'Platform is not supported, event unrecognised.'
}
