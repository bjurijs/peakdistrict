import Core from './Core'
import platforms from './list'
import { get } from '../misc'

export default class Lex extends Core {
  constructor (event, callback) {
    super(event, callback)
    this.newEvent(event)

    this.response = {
      'dialogAction': {
        'type': 'Close',
        'fulfillmentState': 'Fulfilled',
        'message': {
          'contentType': 'PlainText'
        }
      }
    }
  }

  newEvent (event) {
    this.platform = platforms.PLATFORM_LEX
    this.source = platforms.SRC_DEFAULT

    this.attributes = event.sessionAttributes || {}
    this.userId = event.userId || undefined

    this.request = undefined

    this.intentname = get(event, 'currentIntent.name')
    this.slots = []

    let slots = get(event, 'currentIntent.slots')

    if (slots) {
      let slotcount = Object.keys(event.currentIntent.slots).length || 0

      for (let i = 0; i < slotcount; i++) {
        const slotname = Object.keys(event.currentIntent.slots)[i]
        const slotvalue = event.currentIntent.slots[slotname]

        this.slots[slotname] = slotvalue || undefined
      }
    }

    let ignoreState = ['AMAZON.YesIntent', 'AMAZON.NoIntent', 'AMAZON.HelpIntent', 'Unhandled', 'SessionEndedRequest']
    if (ignoreState.indexOf(this.intentname) < 0) {
      this.attributes['state'] = this.intentname || this.request
    }

    this.input = event.inputTranscript || undefined
    this.dialogstate = undefined
  }

  speak (text) {
    this.response.dialogAction.message.content = text
    return this
  }

  Ready () {
    this.response.sessionAttributes = this.attributes
    this.callback(null, this.response)
    return this
  }
}
