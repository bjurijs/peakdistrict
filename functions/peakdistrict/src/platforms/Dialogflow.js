import Core from './Core'
import platforms from './list'
const misc = require('../misc')

export default class Dialogflow extends Core {
  constructor (event, callback) {
    super(event, callback)
    this.newEvent(event)

    this.response = {
      'fulfillmentText': ''
    }
  }

  newEvent (event) {
    this.platform = platforms.PLATFORM_DIALOGFLOW
    this.source = platforms.SRC_DEFAULT

    this.responseId = event.responseId || {}
    this.request = undefined
    this.attributes = {}

    this.intentname = misc.get(event, 'queryResult.action')
    this.slots = misc.get(event, 'queryResult.parameters') || {}

    if (event.parameters) {
      let slotcount = Object.keys(event.parameters).length || 0

      for (let i = 0; i < slotcount; i++) {
        const slotname = Object.keys(event.parameters)[i]
        const slotvalue = event.parameters[slotname]

        this.slots[slotname] = slotvalue || undefined
      }
    }

    let ignoreState = ['AMAZON.YesIntent', 'AMAZON.NoIntent', 'AMAZON.HelpIntent', 'Unhandled', 'SessionEndedRequest']
    if (ignoreState.indexOf(this.intentname) < 0) {
      this.attributes['state'] = this.intentname || this.request
    }

    this.accesstoken = undefined
  }

  postprocess (say) {
    return misc.removeSSML(say)
  }

  speak (text) {
    this.response.fulfillmentText = misc.removeSSML(text)
  }

  Ready () {
    this.callback(null, this.response)
  }
}
