import Core from './Core'
import platforms from './list'
import { removeSSML, get } from '../misc'

export default class Alexa extends Core {
  constructor (event, callback) {
    super(event, callback)
    this.newEvent(event)

    this.response = {
      'version': '1.0',
      'sessionAttributes': {},
      'response': {
        'outputSpeech': {
          'type': 'SSML'
        },
        'shouldEndSession': false
      }
    }
  }

  newEvent (event) {
    this.platform = platforms.PLATFORM_ALEXA
    this.source = platforms.SRC_DEFAULT

    this.attributes = get(event, 'session.attributes') || {}
    this.userId = get(event, 'session.user.userId')

    this.request = get(event, 'request.type')

    this.intentname = get(event, 'request.intent.name')
    this.slots = []

    let slots = get(event, 'request.intent.slots')

    if (slots) {
      let slotcount = Object.keys(event.request.intent.slots).length || 0

      for (let i = 0; i < slotcount; i++) {
        const slotname = Object.keys(event.request.intent.slots)[i]
        const slot = event.request.intent.slots[slotname]
        let slotvalue = slot.value

        if (slot.resolutions && slot.resolutions.resolutionsPerAuthority[0] && slot.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH') {
          slotvalue = slot.resolutions.resolutionsPerAuthority[0].values[0].value.name // TODO: multiple resolutions and values?
        }

        this.slots[slotname] = slotvalue || undefined
      }
    }

    let ignoreState = ['Unhandled', 'SessionEndedRequest']
    if (ignoreState.indexOf(this.intentname) < 0) {
      this.attributes['state'] = this.intentname || this.request
    }

    this.input = undefined
    this.dialogstate = get(event, 'request.dialogState')
  }

  card (title, body, image720, image1200) {
    let noSSMLbody = removeSSML(body)

    this.response.response.card = {
      'type': 'Simple',
      'title': title,
      'content': noSSMLbody
    }

    if (image720 || image1200) {
      this.response.response.card = {
        'type': 'Standard',
        'title': title,
        'text': noSSMLbody,
        'image': {
          'smallImageUrl': image720,
          'largeImageUrl': image720
        }
      }
    }

    if (image720 && image1200) {
      this.response.response.card.image = {
        'smallImageUrl': image720,
        'largeImageUrl': image1200
      }
    }
    return this
  }

  reprompt (text) {
    this.response.response.reprompt = {
      'outputSpeech': {
        'type': 'SSML',
        'ssml': '<speak>' + text + '</speak>'
      }
    }
    return this
  }

  speak (text) {
    this.response.response.outputSpeech.ssml = '<speak>' + text + '</speak>'
    return this
  }

  delegate () {
    this.response.response = {
      'directives': [ { 'type': 'Dialog.Delegate' } ],
      'shouldEndSession': false
    }
    return this
  }

  endSession () {
    this.response.response.shouldEndSession = true
    return this
  }

  Ready () {
    this.response.sessionAttributes = this.attributes
    this.callback(null, this.response)
    return this
  }
}
