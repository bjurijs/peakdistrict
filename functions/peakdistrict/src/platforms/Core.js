export default class Core {
  constructor (event, callback) {
    this.event = event
    this.callback = callback
  }

  card () {
    /**
     * Used only for Alexa skills
     * Documentation: https://developer.amazon.com/docs/custom-skills/request-and-response-json-reference.html#response-format
     */
  }

  reprompt () {
    /**
     * Used only for Alexa skills
     * Documentation: https://developer.amazon.com/docs/custom-skills/request-and-response-json-reference.html#response-format
     */
  }

  endSession () {
    /**
     * Used only for Alexa skills
     * Documentation: https://developer.amazon.com/docs/custom-skills/request-and-response-json-reference.html#response-format
     */
  }
}
