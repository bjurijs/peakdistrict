import Alexa from './Alexa'
import Lex from './Lex'
import Dialogflow from './Dialogflow'
import { get } from '../misc'
import dict from '../dict'

export default function (event) {
  let platforms = [
    { name: 'Alexa', trigger: get(event, 'session.application.applicationId'), handler: new Alexa(event) },
    { name: 'Lex', trigger: get(event, 'bot'), handler: new Lex(event) },
    { name: 'Dialogflow', trigger: get(event, 'queryResult'), handler: new Dialogflow(event) }
  ]
  let platform = platforms.find((platform) => platform.trigger)
  if (platform) {
    return platform.handler
  } else {
    throw new Error(dict.ERROR_PLATFORM_UNSUPPORTED)
  }
}
