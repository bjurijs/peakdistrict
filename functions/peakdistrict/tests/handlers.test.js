import { expect } from 'chai'
import Handlers from '../src/handlers'

const Handler1 = { helloworld (channel) {}, bye (channel) { return true }, Unhandled (channel) { return false } }
const Handler2 = {}

let pd
let channel = { attributes: { state: '' } }

describe('Intent Handlers', () => {
  beforeEach(function () {
    pd = new Handlers()
    pd.handlers.push(Handler1, Handler2)
  })

  afterEach(function () {
    pd.handlers = []
    channel.attributes['state'] = ''
  })

  it('returns handler that matches state (function)', () => {
    let state = 'helloworld'
    const requestHandler = pd.requestHandler(state)
    expect(requestHandler).to.equal(Handler1)
  })

  it('triggers right function in handler', () => {
    // comparing return values
    channel.attributes['state'] = 'bye'
    let getfunc = pd.getHandler(channel)
    let expected = pd.handlers[0].bye(channel)
    expect(getfunc).to.equal(expected)
  })

  it('triggers "Unhandled" function if unknown request (intent) comes in', () => {
    // comparing return values
    channel.attributes['state'] = 'wrong'
    let getfunc = pd.getHandler(channel)
    let expected = pd.handlers[0].Unhandled(channel)
    expect(getfunc).to.equal(expected)
  })
})
