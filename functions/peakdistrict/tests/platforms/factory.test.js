import { expect } from 'chai'
import factory from '../../src/platforms/factory'
import dict from '../../src/dict'

let eventLex = { bot: true }
let eventAlexa = { session: { application: { applicationId: true } } }
let eventDialogflow = { queryResult: true }
let eventUnknown = { unknown: 'unknown' }

describe('Detecting Platforms', () => {
  it('should detect alexa', () => {
    expect(factory(eventAlexa).platform).to.equal(1)
  })
  it('should detect lex', () => {
    expect(factory(eventLex).platform).to.equal(2)
  })
  it('should detect dialogflow', () => {
    expect(factory(eventDialogflow).platform).to.equal(3)
  })
  it('unknown event returns error', () => {
    expect(() => factory(eventUnknown)).to.throw(Error, dict.ERROR_PLATFORM_UNSUPPORTED)
  })
})
