import { expect } from 'chai'
import removeSSML from '../src/utils/remove-ssml'
import checkPath from '../src/utils/check-path'

const fxt = {
  'session': {
    'application': {
      'applicationId': 'amzn1.ask.skill.111-222-333'
    },
    'list': [
      {
        'listitem1': 'val1',
        'listitem2': 'val2'
      },
      {
        'listitem3': 'val3',
        'listitem4': 'val4'
      }
    ]
  }
}

describe('Utils', function () {
  it('filter SSML tags', () => {
    let input = '<speak>Hello <break time="3s"/>World</speak>'
    let filtered = 'Hello World'
    let output = removeSSML(input)
    expect(output).to.equal(filtered)
  })

  it('path correct, should return value', () => {
    let id = checkPath(fxt, 'session.application.applicationId')
    let output = 'amzn1.ask.skill.111-222-333'
    expect(id).to.equal(output)
  })

  it('path contains undefined keys, should return undefined', () => {
    let id = checkPath(fxt, 'session.doesntexist.applicationId')
    expect(id).to.equal(undefined)
  })
})
