
const context = {
  'succeed': function (data) {
    console.log(JSON.stringify(data, null, '\t'))
  },

  'fail': function (err) {
    console.log('context.fail occurred')
    console.log(JSON.stringify(err, null, '\t'))
  }
}

module.exports = context
