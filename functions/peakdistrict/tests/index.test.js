
/**
 * Testing if correct platform detected. All local.
 * Checking if "channel.platform" set to right constant.
 *
 */

const { expect } = require('chai')
const index = require('../src/index')

describe('Registering handlers', function () {
  const testHandler = {

    HelloWorld: function () {
      console.log('Hello World')
    },

    ByeWorld: function () {
      console.log('Bye World')
    }

  }

  it('Should register handlers and get requested intent', (done) => {
    let channel = {}
    channel.intentname = 'HelloWorld'
    channel.attributes = {}
    channel.attributes['state'] = channel.intentname

    index.registerHandlers(testHandler)
    index.getHandler(channel)

    // let output = []
    // output = { HelloWorld: [Function: HelloWorld], ByeWorld: [Function: ByeWorld] }

    // expect(index.handlers).to.eql(output)
    done()
  })
})
