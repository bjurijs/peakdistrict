const fs = require('fs')

const lambda = require('./helloworld/index')
const context = require('./_context')

let event = JSON.parse(fs.readFileSync('./events/alexa/WelcomeIntent.json', 'utf-8'))

function callback (error, data) {
  if (error) {
    console.log(error)
  } else {
    console.log(data)
  }
}

lambda['handler'](event, context, callback)
